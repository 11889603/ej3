En en repositorio expuesto o bare, todos los usuarios que estén conectados a él pude hacer hacer pull y clonarlo, es decir descargarse los proyectos que se encuentren en el, para luego modificarlos, y haciendo push pueden volver a subirlo ya modificado a este mencionado repositorio expuesto o bare, para que luego el mismo u otros usuarios realicen la misma operacion.

Sin embargo en los repositorios que no lo son , los usuarios que estén conectados a él solo podran hacer pull, es decir solo clonarlo. Podrán hacer cambios en él, pero no podrán hacer push y subirlo modificado a este repositorio no expuesto, a no ser que el administrador del repositorio le diera permiso para ello

Se utiliza uno u otro dependiendo de las caracteristicas del proyecto y los permisos que se quieran dar de lectura y/o escritura
